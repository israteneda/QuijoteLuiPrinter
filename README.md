# QuijoteLuiPrinter
Aplicación para generar RIDE en formato PDF de los comprobantes electrónicos del SRI Ecuador

## Para compilar.
```
$ ant
```
## Para publicar en en repositorio Maven Local.
```
$ mvn install:install-file -Dfile=./dist/QuijoteLuiPrinter-1.2.jar -DgroupId=com.quijotelui.printer -DartifactId=QuijoteLuiPrinter -Dversion=1.2 -Dpackaging=jar
```
### Documentación
https://www.allku.expert/quijotelui-printer/
